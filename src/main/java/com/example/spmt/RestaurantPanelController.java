package com.example.spmt;

import com.example.spmt.Scheduler.Scheduler;
import com.example.spmt.admin.restaurant.RestaurantRepository;
import com.example.spmt.order.OrderCreator;
import com.example.spmt.order.OrderProxy;
import com.example.spmt.order.OrderRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by dd on 2018-01-13.
 */
@Controller
public class RestaurantPanelController {

    private RestaurantRepository restaurantRepository;

    private OrderRepository orderRepository;

    private OrderCreator orderCreator;


    public RestaurantPanelController(RestaurantRepository restaurantRepository, OrderRepository orderRepository,
                                     OrderCreator orderCreator){
        this.restaurantRepository = restaurantRepository;
        this.orderRepository = orderRepository;
        this.orderCreator = orderCreator;
        //this.scheduler = new Scheduler(restaurantRepository,);
    }

    @RequestMapping(value = "/restaurant/{someID}", method = RequestMethod.GET)
    public String getAttr(@PathVariable(value="someID") String id, Model restaurant, Model orderProxy, Model ordersList){
        restaurant.addAttribute("restaurant", this.restaurantRepository.findOne(Long.parseLong(id)));
        orderProxy.addAttribute("orderProxy", new OrderProxy());
        ordersList.addAttribute("ordersList", this.orderRepository.findAll());
        return "/singlerestaurant";
    }

    @RequestMapping(value = "/createOrder", method = RequestMethod.POST)
    public String createOrderPOST(OrderProxy orderProxy){
        this.orderCreator.saveOrder(orderProxy);
        return "redirect:/restaurant/"+orderProxy.getRestaurantId();
    }

}
