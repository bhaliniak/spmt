package com.example.spmt.Scheduler;

public class GraphElement {
	private String startPoint;
	private String destinationPoint;
	private Long distance;

	public GraphElement(String startPoint, String destinationPoint, Long distance) {
		this.startPoint = startPoint;
		this.destinationPoint = destinationPoint;
		this.distance = distance;
	}

	public Long getDistance() {
		return distance;
	}

	public String getStartPoint() {
		return startPoint;
	}

	public String getDestinationPoint() {
		return destinationPoint;
	}
}
