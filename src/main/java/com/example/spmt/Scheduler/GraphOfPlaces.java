package com.example.spmt.Scheduler;

import com.example.spmt.distance.DistanceCalculator;
import com.example.spmt.distance.MainDistanceElement;
import com.google.gson.Gson;

import java.util.ArrayList;

public class GraphOfPlaces {
	private ArrayList<GraphElement> graph;
	private ArrayList<String> places;
	private DistanceCalculator distanceCalculator;

	public GraphOfPlaces() {
		distanceCalculator = new DistanceCalculator();
		graph = new ArrayList<GraphElement>();
		places = new ArrayList<String>();
	}

	public void addPlace(String place) {
		for (String p : places) {
			graph.add(new GraphElement(p, place, calculateDistance(p, place)));
			graph.add(new GraphElement(place, p, calculateDistance(place, p)));
		}
		places.add(place);
	}

	public void removePlace(String place) {
		for (GraphElement g : graph) {
			if (g.getStartPoint() == place || g.getDestinationPoint() == place)
				graph.remove(g);
		}
		places.remove(place);
	}

	public Long getTheDistance(String point1, String point2) {
		//System.out.println("Point1 " + point1);
		//System.out.println("Point2 " + point2);
		Long distance = new Long(0);
		for (GraphElement g : graph) {
			if (g.getStartPoint().equals(point1) && g.getDestinationPoint().equals(point2)) {
				distance = g.getDistance();
				break;
			}
		}
		return distance;
	}

	private Long calculateDistance(String point1, String point2) {
		String jsonFromGoogleMapsApi = this.distanceCalculator.getDistanceBetweetTwoPoints(point1, point2);
		MainDistanceElement elementFromMap = parseJsonToMapElement(jsonFromGoogleMapsApi);
		Long durationValue = elementFromMap.getDurationValue();
		// Long distanceValue = elementFromMap.getDistanceValue();
		return durationValue;
	}

	private MainDistanceElement parseJsonToMapElement(String jsonFromGoogleMapsApi) {
		return new Gson().fromJson(jsonFromGoogleMapsApi, MainDistanceElement.class);
	}

}
