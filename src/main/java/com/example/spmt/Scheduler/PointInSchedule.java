package com.example.spmt.Scheduler;

public class PointInSchedule {
	private static int counter = 0;
	private Integer pointId;
	private Integer orderId;
	private boolean isRestaurant;
	private String address;

	public PointInSchedule() {
	}

	public PointInSchedule(boolean isRestaurant, String address, Integer orderId) {
		this.isRestaurant = isRestaurant;
		this.address = address;
		this.orderId = orderId;
		setPointId(++counter);
	}
	
	public Integer getPointId() {
		return pointId;
	}
	
	public void setPointId(Integer pointId) {
		this.pointId = pointId;
	}

	public Integer getOrderId() {
		return orderId;
	}
	
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public boolean isRestaurant() {
		return isRestaurant;
	}

	public String getAddress() {
		return address;
	}

}
