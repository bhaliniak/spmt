package com.example.spmt.Scheduler;

import com.example.spmt.admin.restaurant.Restaurant;
import com.example.spmt.admin.supplier.Supplier;
import com.example.spmt.admin.restaurant.RestaurantRepository;
import com.example.spmt.admin.supplier.SupplierRepository;
import com.example.spmt.order.ClientOrder;
import com.example.spmt.order.OrderProxy;
import com.example.spmt.order.OrderRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class Scheduler {
	private double currentTemp = 50;
	private double minTemp = 0.0002;
	private double coolingRate = 0.03;
	private int k_max = 40;
	private int newestOrderId = 0;;
	private Random generator;
	private GraphOfPlaces graph;
	private ArrayList<SupliersManager> supliers; 
	public ArrayList<SupliersManager> bestSolution;
	private RestaurantRepository restaurantRepository;
	private SupplierRepository supplierRepository;
	private OrderRepository orderRepository;
	private Map<Integer, Long> listIndexToSuplierId;


	public Scheduler(RestaurantRepository restaurantRepository, SupplierRepository supplierRepository,
			OrderRepository orderRepository) {
		
		this.restaurantRepository = restaurantRepository;
		this.supplierRepository = supplierRepository;
		this.orderRepository = orderRepository;
		this.generator = new Random();
		this.graph = new GraphOfPlaces();
		this.supliers = new ArrayList<SupliersManager>();
		this.bestSolution = new ArrayList<SupliersManager>();
		this.listIndexToSuplierId = new TreeMap<Integer, Long>();
		
		List<Restaurant> restaurantsList = (List<Restaurant>) restaurantRepository.findAll();
		for (Restaurant r : restaurantsList) {
			graph.addPlace(r.getAddress());
		}
		
		List<Supplier> supplierList = (List<Supplier>) supplierRepository.findAll();
		
		if (!supplierList.isEmpty()) {
			//String s = supplierList.get(0).getBaseAddress();
			graph.addPlace(supplierList.get(0).getBaseAddress());
			for (int i = 0; i < supplierList.size(); ++i) {
				listIndexToSuplierId.put(i, supplierList.get(i).getId());
				supliers.add(new SupliersManager(supplierList.get(i).getId(), supplierList.get(i).getName()));
			}
		}

	}

	public void CalculateNewSchedule(OrderProxy order) {

//		graph = new GraphOfPlaces();
//		List<Restaurant> restaurantsList = (List<Restaurant>) restaurantRepository.findAll();
//		for (Restaurant r : restaurantsList) {
//			graph.addPlace(r.getAddress());
//		}
//		
//		List<Supplier> supplierList = (List<Supplier>) supplierRepository.findAll();
//		
//		if (!supplierList.isEmpty()) {
//			//String s = supplierList.get(0).getBaseAddress();
//			graph.addPlace(supplierList.get(0).getBaseAddress());
//			supliers = new ArrayList<SupliersManager>();
//			for (int i = 0; i < supplierList.size(); ++i) {
//				listIndexToSuplierId.put(i, supplierList.get(i).getId());
//				supliers.add(new SupliersManager(supplierList.get(i).getId(), supplierList.get(i).getName()));
//			}
//		}
		
		currentTemp = 30;
		graph.addPlace(order.calculateAddress());
		PointInSchedule restaurantDestinationPoint = new PointInSchedule(true,
				restaurantRepository.findOne(Long.parseLong(order.getRestaurantId())).getAddress(), newestOrderId);
		PointInSchedule clientDestinationPoint = new PointInSchedule(false, order.calculateAddress(), newestOrderId);
		supliers.get(0).addDestinationPoint(restaurantDestinationPoint);
		supliers.get(0).addDestinationPoint(clientDestinationPoint);
		newestOrderId++;
		simulateAnnealing(this.supliers);
		CalculateEnergy(bestSolution);
		this.printBestSolution();
	}
	
	public void printScheduler() {
		for (SupliersManager suplier : supliers) {
			System.out.println(suplier.getDistance());
			suplier.printDestinationPoints();
		}
	}
	
	public void printBestSolution() {
		for (SupliersManager suplier : bestSolution) {
			System.out.println("Dystans do przebydzia dla dostawcy " + suplier.getName() +  ": " + suplier.getDistance());
			System.out.println("Punkty: ");
			suplier.printDestinationPoints();
		}
	}

	private double CalculateEnergy(ArrayList<SupliersManager> supliers) {
		double result = 0;
		ArrayList<Double> allSupliersDistance = new ArrayList<Double>();
		for (SupliersManager suplier : supliers) {
			Double summarySuplierDistance = 0.0;

			for (int i = 0; i < suplier.numberOfDestinationPoints() - 1; i++) {
				summarySuplierDistance += graph.getTheDistance(suplier.getDestinationPoint(i).getAddress(), 
											suplier.getDestinationPoint(i + 1).getAddress()).intValue();
			}
			//result += summarySuplierDistance;
			suplier.setDistance(summarySuplierDistance);
			allSupliersDistance.add(summarySuplierDistance);
		}
		result = Collections.max(allSupliersDistance);
		System.out.println("allSupliersDistance " + allSupliersDistance);
		return result;
	}
	
	private void swapSupliersJobs(SupliersManager donor, SupliersManager receiver, Boolean isReceiverEmpty) {
		Integer indexOfDonorJob = donor.getRandomDestinationPointIndex();
		PointInSchedule job1 = donor.getDestinationPoint(indexOfDonorJob);
		PointInSchedule job2 = new PointInSchedule();
		if (job1.isRestaurant()) {
			for (int i = 0; i < donor.numberOfDestinationPoints(); ++i) {
				if (donor.getDestinationPoint(i).getOrderId() == job1.getOrderId() && !donor.getDestinationPoint(i).isRestaurant()) {
					job2 = donor.getDestinationPoint(i);
					break;
				}
			}
			if(isReceiverEmpty) {
				receiver.addDestinationPoint(job1);
				receiver.addDestinationPoint(job2);
			}
			else {
				//dodaj restauracje w losowe miejsce
				int newIndexForRest = receiver.getRandomDestinationPointIndex();
				int receiverSize = receiver.numberOfDestinationPoints();
				//System.out.println("R newIndexForRest" + newIndexForRest);
				receiver.setDestinationPoint(newIndexForRest, job1);
				// restauracja byla dodana na koniec to dodaj klienta jako ostatni element
				if(newIndexForRest == receiverSize -1) {
					receiver.addDestinationPoint(job2);
				}
				else {
					int newIndexForClient = generator.nextInt((receiverSize - newIndexForRest) + 1) + newIndexForRest+1;
					receiver.setDestinationPoint(newIndexForClient, job2);
				}
			}
			donor.removeDestinationPoint(job1);
			donor.removeDestinationPoint(job2);
		} else {
			for (int i = 0; i < donor.numberOfDestinationPoints(); ++i) {
				if (donor.getDestinationPoint(i).getOrderId() == job1.getOrderId() && donor.getDestinationPoint(i).isRestaurant()) {
					job2 = donor.getDestinationPoint(i);
					break;
				}
			}
			if(isReceiverEmpty) {
				receiver.addDestinationPoint(job2);
				receiver.addDestinationPoint(job1);
			}
			else {
				int newIndexForRest = receiver.getRandomDestinationPointIndex();
				int receiverSize = receiver.numberOfDestinationPoints();
				//System.out.println("NR receiverSize" + receiverSize);
				receiver.setDestinationPoint(newIndexForRest, job2);
				
				if(newIndexForRest == receiverSize-1) {
					receiver.addDestinationPoint(job1);
				}
				else {
					int newIndexForClient = generator.nextInt((receiverSize - newIndexForRest) + 1) + newIndexForRest+1;
					receiver.setDestinationPoint(newIndexForClient, job1);
				}
			}
			donor.removeDestinationPoint(job1);
			donor.removeDestinationPoint(job2);
		}
	}
	
	private void swapBetweenSuppliers(SupliersManager firstSuplier, SupliersManager secondSuplier) {

		if(firstSuplier.isEmpty() && secondSuplier.isEmpty()) {
			return;
		}
		else if(firstSuplier.isEmpty() && !secondSuplier.isEmpty()) {
			swapSupliersJobs(secondSuplier, firstSuplier, true);
		}
		else if(!firstSuplier.isEmpty() && secondSuplier.isEmpty()){
			swapSupliersJobs(firstSuplier, secondSuplier, true);
		}
		else {
			swapSupliersJobs(firstSuplier, secondSuplier, false);
		}
	}
	
	private static double acceptanceProbability(double energy, double newEnergy, double temperature) {
		// If the new solution is better, accept it
		if (newEnergy < energy) {
			return 1.0;
		}
		// If the new solution is worse, calculate an acceptance probability
		return Math.exp(-(energy - newEnergy) / temperature);
	}
	
	private void createBestSolution(ArrayList<SupliersManager> supliers) {
		this.bestSolution = new ArrayList<SupliersManager>();
		int counter = 0;
		for (SupliersManager suplier : supliers) {
			SupliersManager sup = new SupliersManager(suplier.getId(), suplier.getName(), suplier.getDestinationPoints());
			this.bestSolution.add(counter, sup);
			counter ++;
		}
	}

	private void simulateAnnealing(ArrayList<SupliersManager> currentSol) {
		createBestSolution(currentSol);
		while (currentTemp > minTemp) {
			
			ArrayList<SupliersManager> newSolution = currentSol;
			double currentEnergy = CalculateEnergy(newSolution);
			System.out.println("Current Energy: " + currentEnergy);
			ArrayList<Integer> supliersIndex = new ArrayList<Integer>();
			for (int i = 0; i < newSolution.size(); ++i) {
				if (newSolution.get(i).numberOfDestinationPoints() != 0) {
					supliersIndex.add(i);
				}
			}
			
			Integer supliersAmount = newSolution.size();
			Integer donorIndex;
			Integer receiverIndex;
			SupliersManager donor;
			if(supliersAmount >= 3) {
				donorIndex = supliersIndex.get(generator.nextInt(supliersAmount-1));
				donor = newSolution.get(donorIndex);
				newSolution.remove(donor);
				receiverIndex = generator.nextInt(supliersAmount-2);
				newSolution.add(donorIndex, donor);
			}
			else if(supliersAmount == 2) {
				donorIndex = generator.nextInt(supliersAmount);
				if(donorIndex==1)
					receiverIndex = 0;
				else
					receiverIndex=1;
			}
			else if (supliersAmount == 1){
				donorIndex = 0;
				receiverIndex = 0;
			}
			else {
				donorIndex = 0;
				receiverIndex = 0;
			}
			swapBetweenSuppliers(newSolution.get(donorIndex), newSolution.get(receiverIndex));
			double newSolutionEnergy = CalculateEnergy(newSolution);
			
			System.out.println("NewSolutionEnergy: " + newSolutionEnergy);
			// Decide if we should accept the newSolution
			if (acceptanceProbability(currentEnergy, newSolutionEnergy, currentTemp) > Math.random()) {
				supliers = newSolution;
			}

			if (newSolutionEnergy < currentEnergy) {
				createBestSolution(newSolution);
				//bestSolution = (ArrayList<SupliersManager>) supliers.clone();
			}

			// Cool system
			currentTemp *= 1 - coolingRate;
		}
	}
}
