package com.example.spmt.Scheduler;

import java.util.ArrayList;

public class SupliersManager {
	private String name;
	private Long id;
	private ArrayList<PointInSchedule> destinationPoints;
	private double distance; // zmienna przechowujaca wyliczony dystans z punktow

	public SupliersManager() {
		this.destinationPoints = new ArrayList<PointInSchedule>();
		this.distance = 0;
	}
	
	public SupliersManager( Long id, String name, ArrayList<PointInSchedule> destPoints) {
		this.name = name;
		this.id = id;
		this.destinationPoints = (ArrayList<PointInSchedule>) destPoints.clone();
	}
	
	public SupliersManager(Long id, String name) {
		this.destinationPoints = new ArrayList<PointInSchedule>();
		this.distance = 0;
		this.name = name;
		this.id = id;
	}
	
	public Long getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public void addDestinationPoint(PointInSchedule destinationPoint) {
		destinationPoints.add(destinationPoint);
		// modyfikacja punktow, wyzeruj dystans
		distance = 0;
	}
	
	public void removeDestinationPoint(PointInSchedule destinationPoint) {
		if(!this.destinationPoints.isEmpty()) {
			this.destinationPoints.remove(destinationPoint);
		}
	}
	
	public ArrayList<PointInSchedule> getDestinationPoints(){
		return destinationPoints;
	}

	public PointInSchedule getDestinationPoint(Integer index) {
		PointInSchedule result = null;
		if(!destinationPoints.isEmpty() && index != null && index < this.numberOfDestinationPoints()) {
			result = destinationPoints.get(index);
		}
		return (PointInSchedule) result;
	}

	public void setDestinationPoint(int destinationPosition, PointInSchedule destinationPoint) {
		destinationPoints.add(destinationPosition, destinationPoint);
		// modyfikacja punktow, wyzeruj dystans
		distance = 0;
	}
	
	public boolean isEmpty() {
		return this.numberOfDestinationPoints() == 0;
	}

	public int numberOfDestinationPoints() {
		return destinationPoints.size();
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public Integer getRandomDestinationPointIndex() {
		Integer randomIndex = null;
		if(!destinationPoints.isEmpty()) {
			randomIndex = (int) (this.numberOfDestinationPoints() * Math.random());
		}
		return randomIndex;
	}

	public void printDestinationPoints() {
		for (PointInSchedule point : destinationPoints) {
			System.out.println(point.getPointId() + " - " + point.getOrderId() + " - " + point.isRestaurant() + " - " + point.getAddress());
		}
	}
}
