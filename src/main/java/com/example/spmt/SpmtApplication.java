package com.example.spmt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpmtApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpmtApplication.class, args);
	}
}
