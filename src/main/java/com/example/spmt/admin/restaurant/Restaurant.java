package com.example.spmt.admin.restaurant;

import com.example.spmt.order.ClientOrder;

import javax.persistence.*;
import java.util.List;

/**
 * Created by dd on 2018-01-13.
 */
@Entity
public class Restaurant {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    private String city;

    private String street;

    private String streetNumber;

    private String phoneNumber;

    private String address;

    @OneToMany(mappedBy = "restaurant", cascade = CascadeType.ALL)
    private List<ClientOrder> clientOrderList;

    public void calculateAddress(){
        this.address = this.street + "+" + this.streetNumber + "," + this.city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<ClientOrder> getClientOrderList() {
        return clientOrderList;
    }

    public void setClientOrderList(List<ClientOrder> clientOrderList) {
        this.clientOrderList = clientOrderList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
