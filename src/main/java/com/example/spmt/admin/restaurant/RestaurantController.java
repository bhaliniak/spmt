package com.example.spmt.admin.restaurant;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by dd on 2018-01-13.
 */
@Controller
public class RestaurantController {

    private RestaurantRepository restaurantRepository;

    public RestaurantController(RestaurantRepository restaurantRepository){
        this.restaurantRepository = restaurantRepository;
    }

    @RequestMapping(value = "/createRestaurant", method = RequestMethod.GET)
    public String createRestaurantGET(Model newRestaurant, Model restaurantsList){
        newRestaurant.addAttribute("newRestaurant", new Restaurant());
        restaurantsList.addAttribute("restaurantsList", this.restaurantRepository.findAll());
        return "/createrestaurant";
    }

    @RequestMapping(value = "/createRestaurant", method = RequestMethod.POST)
    public String createRestaurantPOST(Restaurant restaurant){
        restaurant.calculateAddress();
        this.restaurantRepository.save(restaurant);
        return "redirect:/createRestaurant";
    }

    @RequestMapping(value = "/chooseRestaurant", method = RequestMethod.POST)
    public String choose(@ModelAttribute Restaurant restaurant){
        return "redirect:/restaurant/" + restaurant.getId();
    }

}
