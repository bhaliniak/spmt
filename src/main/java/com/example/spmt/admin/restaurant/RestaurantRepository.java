package com.example.spmt.admin.restaurant;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by dd on 2018-01-13.
 */
@Repository
public interface RestaurantRepository extends CrudRepository<Restaurant, Long>{
}
