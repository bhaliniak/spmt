package com.example.spmt.admin.supplier;

/**
 * Created by dd on 2018-01-13.
 */

import com.example.spmt.order.ClientOrder;

import javax.persistence.*;
import java.util.List;

@Entity
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    private String name;

    private String surname;

    private String phoneNumber;

    private String baseAddress;

    @OneToMany(mappedBy = "supplier", cascade = CascadeType.ALL)
    private List<ClientOrder> clientOrderList;


    public Supplier(){
        this.baseAddress = "Grunwaldzka+10,Wrocław";
    }

    public String getBaseAddress() {
        return baseAddress;
    }

    public List<ClientOrder> getClientOrderList() {
        return clientOrderList;
    }

    public void setClientOrderList(List<ClientOrder> clientOrderList) {
        this.clientOrderList = clientOrderList;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
