package com.example.spmt.admin.supplier;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by dd on 2018-01-13.
 */
@Controller
public class SupplierController {

    private SupplierRepository supplierRepository;

    public SupplierController(SupplierRepository supplierRepository){
        this.supplierRepository = supplierRepository;
    }

    @RequestMapping(value = "/createSupplier", method = RequestMethod.GET)
    public String createSupplierGET(Model newSupplier, Model suppliersList){
        newSupplier.addAttribute("newSupplier", new Supplier());
        suppliersList.addAttribute("suppliersList", this.supplierRepository.findAll());
        return "/createsupplier";
    }

    @RequestMapping(value = "createSupplier", method = RequestMethod.POST)
    public String createSupplierPOST(Supplier supplier){
        this.supplierRepository.save(supplier);
        return "redirect:/createSupplier";
    }

}
