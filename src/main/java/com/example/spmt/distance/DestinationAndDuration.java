package com.example.spmt.distance;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dd on 2018-01-14.
 */
public class DestinationAndDuration {

    @SerializedName("distance")
    DistanceElement distanceElement;

    @SerializedName("duration")
    DurationElement durationElement;

    public DistanceElement getDistanceElement() {
        return distanceElement;
    }

    public void setDistanceElement(DistanceElement distanceElement) {
        this.distanceElement = distanceElement;
    }

    public DurationElement getDurationElement() {
        return durationElement;
    }

    public void setDurationElement(DurationElement durationElement) {
        this.durationElement = durationElement;
    }
}
