package com.example.spmt.distance;

import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import static org.springframework.http.HttpHeaders.USER_AGENT;

/**
 * Created by dd on 2018-01-14.
 */
@Service
public class DistanceCalculator {

    public final static String URL_FIRST_PART = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";
    public final static String URL_DESTINATION = "&destinations=";
    public final static String API_KEY = "&key=AIzaSyBc1ckggXv7x7bFwQbNSkYkKB0ZX8gY_2s";

    public String getDistanceBetweetTwoPoints(String firstAddress, String secondAddress){
            String response = "";
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(buildUrl(firstAddress, secondAddress));
            HttpResponse con = client.execute(request);
            if(con.getStatusLine().getStatusCode() < 400){
                response = readResponse(con);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return response;
    }

    private String readResponse(HttpResponse con) throws IOException {
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(con.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        return result.toString();
    }

    private String buildUrl(String firstAddress, String secondAddress) throws MalformedURLException, UnsupportedEncodingException {
        return URL_FIRST_PART + URLEncoder.encode(firstAddress, "UTF-8") + URL_DESTINATION + URLEncoder.encode(secondAddress, "UTF-8") + API_KEY;
    }

}
