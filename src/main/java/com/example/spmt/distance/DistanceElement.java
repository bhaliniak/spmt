package com.example.spmt.distance;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dd on 2018-01-14.
 */
public class DistanceElement {

    @SerializedName("text")
    private String distanceInString;

    @SerializedName("value")
    private String distanceInMeters;

    public String getDistanceInString() {
        return distanceInString;
    }

    public void setDistanceInString(String distanceInString) {
        this.distanceInString = distanceInString;
    }

    public String getDistanceInMeters() {
        return distanceInMeters;
    }

    public void setDistanceInMeters(String distanceInMeters) {
        this.distanceInMeters = distanceInMeters;
    }
}
