package com.example.spmt.distance;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dd on 2018-01-14.
 */
public class DurationElement {

    @SerializedName("text")
    private String durationInString;

    @SerializedName("value")
    private String durationInSeconds;

    public String getDurationInString() {
        return durationInString;
    }

    public void setDurationInString(String durationInString) {
        this.durationInString = durationInString;
    }

    public String getDurationInSeconds() {
        return durationInSeconds;
    }

    public void setDurationInSeconds(String durationInSeconds) {
        this.durationInSeconds = durationInSeconds;
    }
}
