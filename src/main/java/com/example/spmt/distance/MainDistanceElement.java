package com.example.spmt.distance;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by dd on 2018-01-14.
 */
public class MainDistanceElement {

    @SerializedName("destination_addresses")
    List<String> destination_addresses;

    @SerializedName("origin_addresses")
    List<String> origin_addresses;

    @SerializedName("rows")
    List<RowElement> rowElements;

    public Long getDistanceValue(){
        try{
            return Long.parseLong(this.rowElements.get(0).getDestinationAndDuration().get(0).getDistanceElement().getDistanceInMeters());
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Long getDurationValue(){
        try{
            return Long.parseLong(this.rowElements.get(0).getDestinationAndDuration().get(0).getDurationElement().getDurationInSeconds());
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public List<String> getDestination_addresses() {
        return destination_addresses;
    }

    public void setDestination_addresses(List<String> destination_addresses) {
        this.destination_addresses = destination_addresses;
    }

    public List<String> getOrigin_addresses() {
        return origin_addresses;
    }

    public void setOrigin_addresses(List<String> origin_addresses) {
        this.origin_addresses = origin_addresses;
    }

    public List<RowElement> getRowElements() {
        return rowElements;
    }

    public void setRowElements(List<RowElement> rowElements) {
        this.rowElements = rowElements;
    }
}
