package com.example.spmt.distance;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by dd on 2018-01-14.
 */
public class RowElement {

    @SerializedName("elements")
    List<DestinationAndDuration> destinationAndDuration;

    public List<DestinationAndDuration> getDestinationAndDuration() {
        return destinationAndDuration;
    }

    public void setDestinationAndDuration(List<DestinationAndDuration> destinationAndDuration) {
        this.destinationAndDuration = destinationAndDuration;
    }
}
