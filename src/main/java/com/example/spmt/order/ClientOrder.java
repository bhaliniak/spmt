package com.example.spmt.order;

import com.example.spmt.admin.restaurant.Restaurant;
import com.example.spmt.admin.supplier.Supplier;

import javax.persistence.*;
import java.awt.*;

/**
 * Created by dd on 2018-01-13.
 */
@Entity
public class ClientOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;

    private String price;

    private String date;

    private String clientAddressCity;

    private String clientAddressStreet;

    private String clientAddressStreetNumber;

    private int orderNumber;

    private boolean isInProgress;

    private boolean isEnded;

    private String clientAddress;

    @ManyToOne
    private Restaurant restaurant;

    @ManyToOne
    private Supplier supplier;

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public boolean isInProgress() {
        return isInProgress;
    }

    public void setInProgress(boolean inProgress) {
        isInProgress = inProgress;
    }

    public boolean isEnded() {
        return isEnded;
    }

    public void setEnded(boolean ended) {
        isEnded = ended;
    }

    public void calculateAddress(){
        this.clientAddress=this.clientAddressStreet + "+" + this.clientAddressStreetNumber + "," + this.clientAddressCity;
    }

    public String getClientAddress(){ return clientAddress;}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public String getClientAddressCity() {
        return clientAddressCity;
    }

    public void setClientAddressCity(String clientAddressCity) {
        this.clientAddressCity = clientAddressCity;
    }

    public String getClientAddressStreet() {
        return clientAddressStreet;
    }

    public void setClientAddressStreet(String clientAddressStreet) {
        this.clientAddressStreet = clientAddressStreet;
    }

    public String getClientAddressStreetNumber() {
        return clientAddressStreetNumber;
    }

    public void setClientAddressStreetNumber(String clientAddressStreetNumber) {
        this.clientAddressStreetNumber = clientAddressStreetNumber;
    }
}
