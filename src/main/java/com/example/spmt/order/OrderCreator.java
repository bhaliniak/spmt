package com.example.spmt.order;

import com.example.spmt.Scheduler.Scheduler;
import com.example.spmt.admin.restaurant.RestaurantRepository;
import com.example.spmt.distance.DistanceCalculator;
import com.example.spmt.distance.MainDistanceElement;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

/**
 * Created by dd on 2018-01-14.
 */
@Service
public class OrderCreator {

    private DistanceCalculator distanceCalculator;

    private RestaurantRepository restaurantRepository;

    private Scheduler scheduler;

    public OrderCreator(DistanceCalculator distanceCalculator, RestaurantRepository restaurantRepository, Scheduler scheduler){
        this.distanceCalculator = distanceCalculator;
        this.restaurantRepository = restaurantRepository;
        this.scheduler = scheduler;
    }

    public void saveOrder(OrderProxy orderProxy){
        String clientAddress = orderProxy.calculateAddress();
        String restaurantAddress = this.restaurantRepository.findOne(Long.parseLong(orderProxy.getRestaurantId())).getAddress();
        String jsonFromGoogleMapsApi = this.distanceCalculator.getDistanceBetweetTwoPoints(restaurantAddress, clientAddress);
        MainDistanceElement elementFromMap = parseJsonToMapElement(jsonFromGoogleMapsApi);
        ClientOrder clientOrder = new ClientOrder();
        this.scheduler.CalculateNewSchedule(orderProxy);
    }

    private MainDistanceElement parseJsonToMapElement(String json){
        return new Gson().fromJson(json, MainDistanceElement.class);
    }

}
