package com.example.spmt.order;

/**
 * Created by dd on 2018-01-13.
 */
public class OrderProxy {

    private String name;

    private String restaurantId;

    private String orderReadyIn;

    private String price;

    private String clientAddressCity;

    private String clientAddressStreet;

    private String clientAddressStreetNumber;

    public String calculateAddress(){
        return this.clientAddressStreet + "+" + this.clientAddressStreetNumber + "," + this.clientAddressCity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getOrderReadyIn() {
        return orderReadyIn;
    }

    public void setOrderReadyIn(String orderReadyIn) {
        this.orderReadyIn = orderReadyIn;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getClientAddressCity() {
        return clientAddressCity;
    }

    public void setClientAddressCity(String clientAddressCity) {
        this.clientAddressCity = clientAddressCity;
    }

    public String getClientAddressStreet() {
        return clientAddressStreet;
    }

    public void setClientAddressStreet(String clientAddressStreet) {
        this.clientAddressStreet = clientAddressStreet;
    }

    public String getClientAddressStreetNumber() {
        return clientAddressStreetNumber;
    }

    public void setClientAddressStreetNumber(String clientAddressStreetNumber) {
        this.clientAddressStreetNumber = clientAddressStreetNumber;
    }
}
