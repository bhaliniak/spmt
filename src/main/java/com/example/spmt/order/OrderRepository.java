package com.example.spmt.order;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by dd on 2018-01-13.
 */
@Repository
public interface OrderRepository extends CrudRepository<ClientOrder, Long>{
}
